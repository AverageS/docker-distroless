FROM python:3.12-slim AS build-env
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt


FROM gcr.io/distroless/python3
COPY --from=build-env /app /app
COPY --from=build-env /usr/local/lib/python3.12/site-packages/ /app/site-packages/
ENV PYTHONPATH /app/site-packages

WORKDIR /app
CMD ["main.py"]
